Hn(y,y0,n) = 1.0/(1.0 + (y/y0)^n)     # negative Hill function
Hp(y,y0,n) = 1.0 - Hn(y,y0,n)         # positive Hill function
HSn(y,y0,n,e) = (1.0-e)+e*Hn(y,y0,n)  # e=1.0 -> negative Hill function, e=0.0 -> 1.0
HSp(y,y0,n,e) = (1.0-e)+e*Hp(y,y0,n)  # e=1.0 -> positive Hill function, e=0.0 -> 1.0

function noise(du,u,p,t)
    # adds a white noise to the equations. Noise level is defined by the variable nlevel
    for i in 1:neqs
        du[i,:,:] = nlevel.*u[i,:,:]
    end
end

function mean_quantiles(sol,n_tr)
    # Gets the quantiles (5%,95%), median and mean. Also returns the fraction of MN cells
    npoints = length(sol.t)
    m = zeros(neqs,5,npoints)
    for j in 1:neqs
        for i in 1:npoints
            m[j,1:3,i] = quantile(sol[j,2:(end-1),2:(end-1),i][:],[0.05,0.5,0.95])
            m[j,4  ,i] = mean(sol[j,2:(end-1),2:(end-1),i][:])
        end
    end
    m[end,5,:] = mean(cdf.(Gamma(1000.0*n_tr,0.0004/n_tr),sol[end,:,:,:]),dims=(1,2))[:]
    return m
end

function plot_expression_withCI(t,m;plt=:none,fillalpha=0.2,color=:salmon,grid=false,ylim=(0,1.15),legend=false,
                                line=:solid,show_thr=true,ylabel="expression level",tr=thr)
    if plt==:none
        plt=plot( [t t],[m[2,:] m[2,:]],fillrange=[m[1,:] m[3,:]],fillalpha=fillalpha,color=color,grid=grid,lw=2.5,
                line=line,ylim=ylim,legend=legend);
    else
        plot!(plt,[t t],[m[2,:] m[2,:]],fillrange=[m[1,:] m[3,:]],fillalpha=fillalpha,color=color,grid=grid,lw=2.5,
                  line=line,ylim=ylim,legend=legend);
    end
    xlabel!(plt,"time (days)")
    xticks!(plt,[4,5,6,7])
    yticks!(plt,[0.0, 0.5, 1.0])
    ylabel!(plt,ylabel)

    if show_thr==true
        lt = length(t)
        plot!(plt, t,tr*ones(lt),lw=0.5,color=:steelblue,legend=false,line=:dash)
        plot!(plt, [t t],[tr*ones(lt) tr*ones(lt)],fillrange=[(tr-0.1)*ones(lt) (tr+0.1)*ones(lt)],
                   fillalpha=0.2,lw=0.5,color=:steelblue,legend=false)
    end
    return plt
end

function plot_fractionCells(tseq,fseq;color=:black,line=:solid)
    plt=plot(tseq,fseq,lw=3.5,xlim=(3.5,7.5),legend=false,grid=false,line=line,
         color=color,ylim=[-0.01,1.01])
    yticks!(plt,[0.0,0.5,1.0])
    xticks!(plt,[4,5,6,7])
    xlabel!(plt,"time (days)")
    ylabel!(plt,"likelihood of MN diff")
    return plt
end

function solve_equations(eqs,noise,v,h,n,e,u0,p;n_tr=1e-2)
    prob = SDEProblem(eqs,noise,u0,tr,p)
    sol = solve(prob,saveat=dt)
    return mean_quantiles(sol,n_tr)
end

function DeltaDiff(t,f)
    c = (f.>0.05).& (f.<0.95)
    return t[c][end] - (t[c])[1]
end

function bifurcation_analysis(v,u0,h,n,e,p)
    rGliR = 0.01:0.02:1.0
    N=1
    p[1]=1.0
    out = zeros(2,length(rGliR))

    h[2]=0.1
    u0[3] = 0.0
    for i in 1:length(rGliR)
        p[2] = rGliR[i]
        prob = ODEProblem(eqs,u0,tr,p)
        sol = solve(prob,saveat=dt)
        out[:,i] = [p[2], sol[3,end]]
    end
    p1D=plot(out[1,:],out[2,:],ylabel="TgfB",xlabel="GliR", lw=3.0,color=:steelblue)
    u0[3]=0.5
    for i in 1:length(rGliR)
        p[2] = rGliR[i]
        prob = ODEProblem(eqs,u0,tr,p)
        sol = solve(prob,saveat=dt)
        out[:,i] = [p[2], sol[3,end]]
    end
    plot!(p1D,out[1,:],out[2,:],lw=3.0,color=:steelblue,legend=false)

    h[2] =0.15
    e[3] = 0.0
    u0[3] = 0.0
    for i in 1:length(rGliR)
        p[2] = rGliR[i]
        prob = ODEProblem(eqs,u0,tr,p)
        sol = solve(prob,saveat=dt)
        out[:,i] = [p[2], sol[3,end]]
    end
    plot!(p1D,out[1,:],out[2,:],lw=3.0,color=:red)
    u0[3]=0.5
    for i in 1:length(rGliR)
        p[2] = rGliR[i]
        prob = ODEProblem(eqs,u0,tr,p)
        sol = solve(prob,saveat=dt)
        out[:,i] = [p[2], sol[3,end]]
    end
    plot!(p1D,out[1,:],out[2,:],lw=3.0,color=:red,legend=false)

    e[3]=0.94
    h[2]=0.2
    u0[3] = 0.0
    for i in 1:length(rGliR)
        p[2] = rGliR[i]
        prob = ODEProblem(eqs,u0,tr,p)
        sol = solve(prob,saveat=dt)
        out[:,i] = [p[2], sol[3,end]]
    end
    plot!(p1D,out[1,:],out[2,:],lw=3.0,color=:black)
    u0[3]=0.5
    for i in 1:length(rGliR)
        p[2] = rGliR[i]
        prob = ODEProblem(eqs,u0,tr,p)
        sol = solve(prob,saveat=dt)
        out[:,i] = [p[2], sol[3,end]]
    end
    plot!(p1D,out[1,:],out[2,:],lw=3.0,color=:black,legend=false)


    yticks!(p1D,[0.0, 0.5, 1.0])
    xticks!(p1D,[0.0, 1.0, 2.0])
    return p1D
end
